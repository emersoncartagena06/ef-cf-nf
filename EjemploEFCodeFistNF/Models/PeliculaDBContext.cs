﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EjemploEFCodeFistNF.Models
{
    public class PeliculaDBContext : DbContext
    {
        public DbSet<Peliculas> Peliculas { get; set; }
        public DbSet<Etiquetas> Etiquetas { get; set; }    
    }
}