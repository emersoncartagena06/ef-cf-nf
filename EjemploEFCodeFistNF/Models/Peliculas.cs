﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EjemploEFCodeFistNF.Models
{
    public class Peliculas
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Titulo { get; set; }

        public DateTime FechaSalida { get; set; }

        [MaxLength(100)]
        public string Genero { get; set; }

        public decimal Taquilla { get; set; }

        //Caso de etiquetas One-to-Many
        /*[Required]
        public Etiquetas etiqueta { get; set; }*/

        //Caso de etiquetas Many-to-Many
        public List<Etiquetas> Etiquetas { get; set; }
    }
}