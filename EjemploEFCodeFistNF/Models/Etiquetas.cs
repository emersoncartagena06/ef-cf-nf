﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EjemploEFCodeFistNF.Models
{
    public class Etiquetas
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        
        //Caso de etiquetas One-to-Many y Many-to-Many
        public List<Peliculas> Peliculas { get; set; }
    }
}