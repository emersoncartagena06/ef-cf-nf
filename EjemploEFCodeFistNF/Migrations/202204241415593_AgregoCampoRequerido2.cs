﻿namespace EjemploEFCodeFistNF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregoCampoRequerido2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Peliculas", "etiqueta_Id", c => c.Int(nullable: false, defaultValueSql: "2"));
            CreateIndex("dbo.Peliculas", "etiqueta_Id");
            AddForeignKey("dbo.Peliculas", "etiqueta_Id", "dbo.Etiquetas", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Peliculas", "etiqueta_Id", "dbo.Etiquetas");
            DropIndex("dbo.Peliculas", new[] { "etiqueta_Id" });
            DropColumn("dbo.Peliculas", "etiqueta_Id");
        }
    }
}
