﻿namespace EjemploEFCodeFistNF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Restore : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Peliculas", "etiqueta_Id", "dbo.Etiquetas");
            DropIndex("dbo.Peliculas", new[] { "etiqueta_Id" });
            DropColumn("dbo.Peliculas", "etiqueta_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Peliculas", "etiqueta_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Peliculas", "etiqueta_Id");
            AddForeignKey("dbo.Peliculas", "etiqueta_Id", "dbo.Etiquetas", "Id", cascadeDelete: true);
        }
    }
}
