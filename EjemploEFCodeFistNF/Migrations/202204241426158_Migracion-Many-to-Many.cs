﻿namespace EjemploEFCodeFistNF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigracionManytoMany : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PeliculasEtiquetas",
                c => new
                    {
                        Peliculas_Id = c.Int(nullable: false),
                        Etiquetas_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Peliculas_Id, t.Etiquetas_Id })
                .ForeignKey("dbo.Peliculas", t => t.Peliculas_Id, cascadeDelete: true)
                .ForeignKey("dbo.Etiquetas", t => t.Etiquetas_Id, cascadeDelete: true)
                .Index(t => t.Peliculas_Id)
                .Index(t => t.Etiquetas_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PeliculasEtiquetas", "Etiquetas_Id", "dbo.Etiquetas");
            DropForeignKey("dbo.PeliculasEtiquetas", "Peliculas_Id", "dbo.Peliculas");
            DropIndex("dbo.PeliculasEtiquetas", new[] { "Etiquetas_Id" });
            DropIndex("dbo.PeliculasEtiquetas", new[] { "Peliculas_Id" });
            DropTable("dbo.PeliculasEtiquetas");
        }
    }
}
