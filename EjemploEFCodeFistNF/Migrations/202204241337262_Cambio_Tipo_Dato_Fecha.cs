﻿namespace EjemploEFCodeFistNF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambio_Tipo_Dato_Fecha : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Peliculas", "FechaSalida", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Peliculas", "FechaSalida", c => c.String());
        }
    }
}
