﻿namespace EjemploEFCodeFistNF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CambiosValidacion : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Peliculas", "Titulo", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Peliculas", "Genero", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Peliculas", "Genero", c => c.String());
            AlterColumn("dbo.Peliculas", "Titulo", c => c.String());
        }
    }
}
